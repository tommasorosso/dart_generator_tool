*Installation*

Requires rust installed and cargo on your path.

cargo install --git https://gitlab.com/tommasorosso/dart_generator_tool

If an installation is already present append --force to the previous command.  

*Usage*

After installation you should have `dart_generator_tool` on your path  
to use it simply type `dart_generator_tool` on your console inside the folder you want the script to look into.  

If you want the scritp to look into other directories use `dart_generator_tool -p PATH_TO_DIRECTORY`

It will search for dart files and process them in parallel to find `//!` comments  
if found it compiles what follows `//!` and replace it with a comment so it will not be compiled next iteration.
If no `//!` is found nothing happens. 


