#[macro_use]
extern crate nom;
mod file_utils;
mod generator;
mod lexer;

extern crate clap;
extern crate rayon;
extern crate walkdir;

use generator::generate_classes;
use lexer::parse_data_constructor;
use rayon::prelude::*;

use clap::{App, Arg};

fn main() {
    let matches = App::new("Dart generator tool")
        .version("0.9")
        .author("Tommaso Rosso tommasorosso1@gmail.com")
        .about("Helps to bring sum types to dart.\nConverts //!  data A = Variant1 | Variant fieldName:FieldType\nhaskell like syntax to dart")
        .arg(
            Arg::with_name("redux")
                .short("r")
                .takes_value(false)
                .required(false)
        )
        .arg(
            Arg::with_name("path")
                .short("p")
                .default_value(".")
                .long("path")
                .help("Path of your dart projects, it scans dirs from there recursively")
                .takes_value(true)
                .required(true)
        ).get_matches();
    let path = matches
        .value_of("path")
        .expect("you should provide path argument");
    let redux = matches.is_present("redux");
    let file_paths =
        file_utils::dart_file_paths(path).expect("there was an error reading files");

    file_paths.par_iter().for_each(|file_name: &String| {
        println!("{}", file_name);

        let res = file_utils::edit_file(&file_name, &|it: String| {
            let lines: Vec<_> = it.lines().collect();
            let mut lines: Vec<_> = lines.iter().map(std::string::ToString::to_string).collect();

            for (index, line) in it.lines().enumerate() {
                if line.replace(" ", "").contains("//!data") {
                    let generated_code = parse_data_constructor(line)
                        .map(|it| generate_classes(it, redux))
                        .unwrap_or_else(|| "".to_string());
                    let line_removed = lines.remove(index);
                    lines.insert(index, line_removed.replace("//!", "//  Generated from: "));
                    lines.insert(index + 1, generated_code);
                } else {
                    continue;
                }
            }
            lines
                .iter()
                .fold("".to_string(), |a, b| format!("{}{}\n", a, b))
        });
        match res {
            Ok(_) => println!("Parsed: {}", file_name),
            Err(e) => println!("There was an error editing {}: {}", file_name, e)
        };
    });
}
