extern crate nom;
use std::vec::Vec;

#[derive(Debug)]
pub struct Constructor {
    pub name: String,
    pub fields: Vec<(String, String)>,
}

#[derive(Debug)]
pub struct DataConstructor {
    pub name: String,
    pub constructors: Vec<Constructor>,
}

pub fn parse_data_constructor(line: &str) -> Option<DataConstructor> {
    if !line.replace(" ", "").contains("//!data") {
        return None;
    }
    Some(data_constructor(line))
}

fn data_constructor(source: &str) -> DataConstructor {
    named!(till_data(&str) -> &str, take_until_and_consume!("data "));
    named!(till_assignment(&str) -> &str, take_until_and_consume!("="));
    named!(till_pipe(&str) -> &str, take_until!("|"));

    let (rem, _keyword) = till_data(source).unwrap();
    let (rem, type_name) = till_assignment(rem).unwrap();

    let constuctors: Vec<Constructor> = rem
        .split('|')
        .map(std::string::ToString::to_string)
        .map(|it| parse_constructor(&it))
        .collect();

    DataConstructor {
        name: type_name.to_string().replace(" ", ""),
        constructors: constuctors,
    }
}

fn parse_constructor(source: &str) -> Constructor {

    fn parse_field(word: &str) -> (String, String) {
        let list: Vec<_> = word.split(':').collect();
        println!("parsing: {}", word);
        (
            list.get(0).expect("field should be in name:Type form").to_string(),
            list.get(1).expect("field should be in name:Type form").to_string()
        )
    }

    let words: Vec<String> = source
        .split_whitespace()
        .map(std::string::ToString::to_string)
        .collect();
    if words.len() <= 1 {
        return Constructor {
            name: words.first().unwrap().clone(),
            fields: vec![],
        };
    }
    let types: Vec<String> = words[1..].to_vec();
    Constructor {
        name: words.first().unwrap().clone(),
        fields: types.iter().map(|it| parse_field(it)).collect(),
    }
}
