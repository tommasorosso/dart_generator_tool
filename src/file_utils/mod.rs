extern crate walkdir;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;

pub fn dart_file_paths(from: &str) -> Option<Vec<String>> {
    use walkdir::WalkDir;

    let mut files: Vec<String> = vec![];

    for entry in WalkDir::new(from)
        .into_iter()
        .filter_map(std::result::Result::ok)
        .filter(|it| it.file_name().to_str().unwrap_or("").contains(".dart"))
    {
        files.push(entry.path().to_str()?.to_string());
    }

    Some(files)
}

pub fn edit_file(path: &str, edit_function: &Fn(String) -> String) -> Result<(), io::Error> {
    let file_path = Path::new(&path);
    let mut src = File::open(&file_path)?;
    let mut data = String::new();
    src.read_to_string(&mut data)?;

    let new_data: String = edit_function(data);

    drop(src); // Close the file early

    // Recreate the file and dump the processed contents to it
    let mut dst = File::create(&file_path)?;
    dst.write_all(new_data.as_bytes())?;

    Ok(())
}
