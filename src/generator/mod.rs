use crate::lexer::{Constructor, DataConstructor};

pub fn generate_classes(data_constructor: DataConstructor, redux: bool) -> String {
    let mut string = "".to_string();

    string += &format!("abstract class {} {{\n", data_constructor.name);
    string += "  A fold<A>(";

    for constructor in data_constructor.constructors.iter() {
        string += &format!(
            "A Function({}) {}, ",
            constructor.name,
            format!("{}Case", constructor.name.to_lowercase())
        );
    }
    string += ") {\n";

    for constructor in data_constructor.constructors.iter() {
        string += &format!(
            "  if(this is {}) {{ return {}(this as {}); }}\n",
            constructor.name,
            format!("{}Case", constructor.name.to_lowercase()),
            constructor.name
        );
    }
    string += "  }\n}\n";

    let mut add_constr = |constructor: &Constructor| {
        string += &format!(
            "class {} extends {} {{\n",
            constructor.name, data_constructor.name
        );
        let names_and_types= &constructor.fields;

        string += &format!("  {}(", constructor.name);
        for (variable_name, _) in names_and_types.clone() {
            string += &format!("this.{}, ", variable_name);
        }
        string += ");";

        for (variable_name, type_name) in names_and_types {
            string += &format!("\n  final {} {};", type_name, variable_name)
        }

        string += "\n}\n";
    };

    for constr in data_constructor.constructors.iter() {
        add_constr(constr);
    }
    if !redux {
        return string;
    }

    //  State
    string += "\n\n\n  // state\n";
    string += &format!("class {}State {{\n", data_constructor.name);
    string += "  // todo\n";
    string += "}\n";

    //  Reducer
    string += "//\n\n\n   reducer\n";
    string += &format!("{}State {}Reducer({}State state, action) =>\n", data_constructor.name, data_constructor.name, data_constructor.name);
    string += "  action.fold(\n";
    for constructor in data_constructor.constructors.iter() {
        string += &format!("    ({} action) => {}State(),\n", constructor.name, data_constructor.name)
    }
    string += "  );\n";

    string
}
